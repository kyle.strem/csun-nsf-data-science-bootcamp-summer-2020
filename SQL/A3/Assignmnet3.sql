/* A. Who are the employees assigned to each project?  Show ProjectID, EmployeeNumber, LastName, FirstName, and OfficePhone */
SELECT
	ASSIGNMENT.ProjectID,
	EMPLOYEE.EmployeeNumber,
	EMPLOYEE.LastName,
	EMPLOYEE.FirstName,
	EMPLOYEE.OfficePhone
FROM ASSIGNMENT
JOIN EMPLOYEE
ON ASSIGNMENT.EmployeeNumber = EMPLOYEE.EmployeeNumber
ORDER BY ASSIGNMENT.ProjectID;

/* B. Who are the employees assigned to each project? Show ProjectID, ProjectName, and Department. Show EmployeeNumber, LastName, FirstName, and OfficePhone. */
SELECT
	PROJECT.ProjectID,
	PROJECT.ProjectName,
	PROJECT.Department,
	EMPLOYEE.EmployeeNumber,
	EMPLOYEE.LastName,
	EMPLOYEE.FirstName,
	EMPLOYEE.OfficePhone
FROM ASSIGNMENT
JOIN PROJECT
ON ASSIGNMENT.ProjectID = PROJECT.ProjectID
JOIN EMPLOYEE
ON ASSIGNMENT.EmployeeNumber = EMPLOYEE.EmployeeNumber
ORDER BY PROJECT.ProjectID;

/* C. Who are the employees assigned to each project? Show ProjectID, ProjectName, Department, and DepartmentPhone.  Show EmployeeNumber, LastName, FirstName, and OfficePhone. Sort by ProjectID in ascending order. */
SELECT
	P.ProjectID,
	P.ProjectName,
	P.Department,
	D.DepartmentPhone,
	E.EmployeeNumber,
	E.LastName,
	E.FirstName,
	E.OfficePhone
FROM ASSIGNMENT
JOIN PROJECT AS P
ON ASSIGNMENT.ProjectID = P.ProjectID
JOIN EMPLOYEE AS E
ON ASSIGNMENT.EmployeeNumber = E.EmployeeNumber
JOIN DEPARTMENT AS D
ON P.Department = D.DepartmentName
ORDER BY P.ProjectID;

/* D. Who are the employees assigned to projects run by the Sales and Marketing Department? Show ProjectID, ProjectName, Department, and DepartmentPhone. Show EmployeeNumber, LastName, FirstName, and OfficePhone.  Sort by ProjectID in ascending order. */
SELECT
	P.ProjectID,
	P.ProjectName,
	P.Department,
	D.DepartmentPhone,
	E.EmployeeNumber,
	E.LastName,
	E.FirstName,
	E.OfficePhone
FROM ASSIGNMENT AS A
JOIN PROJECT AS P
ON A.ProjectID = P.ProjectID
JOIN EMPLOYEE AS E
ON A.EmployeeNumber = E.EmployeeNumber
JOIN DEPARTMENT AS D
ON P.Department = D.DepartmentName
WHERE P.Department = 'Sales and Marketing'
ORDER BY P.ProjectID;

/* E. How many projects are being run by the Sales and Marketing Department?Be sure to assign an appropriate column name to the computed results. */
SELECT
	COUNT(*) AS NumberOfMarketingDeptProjects
FROM PROJECT
WHERE Department = 'Sales and Marketing';

/* F. What is the total MaxHours of projects being run by the Sales and Marketing Department?Be sure to assign an appropriate column name to the computed results. */
SELECT
	SUM(MaxHours) as TotalMaxHrsForMKTGDeptProjects
FROM PROJECT
WHERE Department = 'Sales and Marketing';

/* G. What is the average MaxHours ofprojects being run by the Sales and Marketing Department? Be sure to assign an appropriate column name to the computed results. */
SELECT
	AVG(MaxHours) as AvgMaxHrsForMKTGDeptProjects
FROM PROJECT
WHERE Department = 'Sales and Marketing';

/* H. How many projects are being run by each department?Be sure to display each DepartmentName and to assign an appropriate column name to the computed results.*/
SELECT
	Department,
	COUNT(ProjectID) AS NumberOfDeptProjects
FROM PROJECT
GROUP BY Department;
