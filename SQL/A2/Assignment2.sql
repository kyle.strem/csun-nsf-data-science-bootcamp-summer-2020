/* A. What projects are in the PROJECT table? Show all information for each project. */
SELECT * FROM PROJECT;
/* B. What are the ProjectID, ProjectName, StartDate, and EndDate values of projects in the PROJECT table? */
SELECT ProjectID, ProjectName, StartDate, EndDate FROM PROJECT;
/* C. What projects in the PROJECT table started before August 1, 2017?  Show all the information for each project. */
SELECT * FROM PROJECT
WHERE StartDate < '2017-08-01';
/* D. What projects in the PROJECT table have not been completed?  Show all the information for each project. */
SELECT * FROM PROJECT
WHERE EndDate IS NULL;
