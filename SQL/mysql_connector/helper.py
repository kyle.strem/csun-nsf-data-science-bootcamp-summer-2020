# This file is to be used as the base connector function to do mysql
# queries for Assignment 4 of Prof. Liu's Section of the NSF Data Science
# Bootcamp CSUN Summer 2020

import mysql.connector

# Function to establish a connection with the mysql server and specified database
def create_connection(user='root', password=None, host='localhost', database=None, use_pure=True):
    return mysql.connector.connect(
        user=user,
        password=password,
        host=host,
        database=database,
        use_pure=True
    )

# Function to create cursor object for database interaction
def get_cursor(connection):
    return connection.cursor(buffered=True)

# Function to execute a query, output will have to be handled by the user
def execute_query(cursor, query_string):
    return cursor.execute(query_string)

# Function to execute a query and handle output
def execute_and_output_query(cursor, query_string):
    cursor.execute(query_string)

    for item in cursor:
        print(item)

# Function to close the cursor
def close_cursor(cursor):
    return cursor.close()

# Function to close the connection
def close_connection(connection):
    return connection.close()
